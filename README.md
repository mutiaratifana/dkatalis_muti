## Instruction to run
- open command on this directory 
```
./parking_lot.sh
```

or

- double click "parking_lot.sh"

<p> When the command has opened, it appears input command to be inputted.</p>

### The commands are:
- to make how many parking area 

```
create_parking_lot {how many capacity u want}
```

- to park a car 
```
park {car number to be parked}
```

- to leave a car which has been parked before
```
leave {car number} {hours}
```

- to check car(s) number that still in the slot.
```
status
```


